from django.contrib import admin
from django.urls import path, include

urlpatterns = [
    path('admin/', admin.site.urls),
    path('watch_me/', include('watch_me.urls')),
    path('', include('watch_me.urls')),
]
