from .models import XVideo, XWatchHistory, XLike, XPlaylist, XplaylistVideo, XTag, XVideoTag, XSearchHistory
from datetime import datetime
from . import queryhelper


def video_from_post(request):
    video = XVideo()
    video.uploader_id = request.session['user_id']
    video.thumbnail = request.FILES['v_tn']
    video.binary = request.FILES['v_video']
    video.name = request.POST['v_name']
    video.category_id = request.POST['category']
    if len(request.POST['v_desc']) < 255:
        video.description = request.POST['v_desc']
    else:
        video.description = request.POST['v_desc'][0:255]
    video.comments_on = 1 if request.POST.get('v_comment', 'off') == 'on' else 0
    video.length = 0
    return video


def save_history(request, id):
    if request.session.get('user_id', 0) != 0:
        history = XWatchHistory()
        history.user_id = request.session['user_id']
        history.w_date = datetime.now()
        history.video_id = id
        history.save()


def is_liked(user, video):
    try:
        v_like = XLike.objects.filter(user__id=user, video__id=video).count()
        if v_like > 0:
            return True
        else:
            return False
    except Exception as ex:
        print(ex)
        return False


def add_video_to_playlist(video, playlist_id=0):
    playlist = XplaylistVideo()
    if playlist_id == 0:
        for res in XPlaylist.objects.raw(queryhelper.SELECT_PLAYLIST_ID_FOR_CATEGORY(video.category_id)):
            playlist.playlist_id = res.id
    else:
        playlist.playlist_id = playlist_id

    playlist.video_id = video.id
    playlist.save()


def save_tags(video, tags):
    if tags == '':
        pass
    if ',' in tags:
        tags = tags.split(',')
    else:
        tags = [tags]

    for tag in tags:
        tag = tag.lower().replace(' ', '')
        if XTag.objects.filter(name=tag).count() > 0:
            xtag = XTag.objects.filter(name=tag).get()
        else:
            xtag = XTag()
            xtag.name = tag
            xtag.save()

        video_tag = XVideoTag()
        video_tag.video_id = video
        video_tag.tag_id = xtag.id
        video_tag.save()


def save_search(keyword, user_id):
    search = XSearchHistory()
    search.user_id = user_id
    search.keyword = keyword
    search.s_date = datetime.now()
    search.save()