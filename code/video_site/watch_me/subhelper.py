from .models import XSubscribe


def is_subbed(from_u, to_u):
    try:
        sub = XSubscribe.objects.filter(from_u_id=from_u, to_id=to_u).all()
        if sub.count() > 0:
            return True
        else:
            return False
    except Exception as e:
        print(e)
        return False


def get_sub(from_u, to_u):
    try:
        sub = XSubscribe.objects.filter(from_u_id=from_u, to_id=to_u).get()
        return sub
    except Exception as e:
        print(e)
        return None
